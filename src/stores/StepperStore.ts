import { action, makeAutoObservable, observable } from "mobx";

export interface IStepperStore {
  totalSteps: number;
  step: number;
}

class StepperStore implements IStepperStore {
  totalSteps;
  @observable step = 0;

  constructor(totalSteps: number) {
    this.totalSteps = totalSteps;
    makeAutoObservable(this);
  }

  @action stepForward = () => {
    ++this.step;
  }

  @action stepBackward = () => {
    --this.step;
  }

  @action reset = () => {
    this.step = 0;
  }
}

export default StepperStore;