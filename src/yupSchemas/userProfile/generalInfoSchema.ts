import * as yup from 'yup';

export const generalInfoSchema = yup.object().shape({
  username: yup.string().required(),
  gender: yup.string().oneOf(['male', 'female']).required(),
  firstName: yup.string(),
  lastName: yup.string(),
  birthDate: yup.date()
});