import * as yup from 'yup';

export const addressInfoSchema = yup.object().shape({
  address: yup.string(),
  country: yup.string(),
  city: yup.string(),
  zip: yup.string()
});