import * as yup from 'yup';
import { phoneRegex } from '../../constants';

export const contactsInfoSchema = yup.object().shape({
  email: yup.string().email(),
  phoneNumber: yup.string().matches(phoneRegex, 'Invalid phone'),
  website: yup.string(),
  aboutMe: yup.string()
});