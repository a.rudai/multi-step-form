import React from 'react';
import { path } from 'ramda';
import { FormikConsumer, FormikProps } from 'formik';
import { FormControl, FormHelperText, Grid, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import { fieldNameToLabel } from '../../helpers/fieldNameToLabel';
import { FormikFieldsProps } from '../../types/FormikFieldsProps';
import { KeyboardDatePicker } from '@material-ui/pickers';

function FormikFields(props: FormikFieldsProps) {
  return (
    <FormikConsumer>
      {(formikProps: FormikProps<any>) => (
        <Grid container className={props.className}>
          {props.fields.map(field => {
            const [key, fieldInfo] = field;
            const error = formikProps.errors[key];
            const hasError: boolean = Boolean(error);

            if (fieldInfo.type === 'date') {
              return (
                <Grid item xs={12} key={key}>
                  <KeyboardDatePicker
                    id={key}
                    name={key}
                    margin="dense"
                    label={fieldNameToLabel(key)}
                    format="MM/dd/yyyy"
                    value={formikProps.values[key]}
                    onBlur={formikProps.handleBlur}
                    onChange={(value) => formikProps.setFieldValue(key, value)}
                    KeyboardButtonProps={{ 'aria-label': 'change date' }}
                    fullWidth
                  />
                </Grid>
              )
            }

            // if white list is not empty return select
            if (fieldInfo._whitelist.list.size !== 0) {
              return (
                <Grid item xs={12} key={key}>
                  <FormControl fullWidth error={hasError}>
                    <InputLabel id={`select-${key}`}>
                      {fieldNameToLabel(key)}
                    </InputLabel>
                    <Select
                      id={key}
                      name={key}
                      margin='dense'
                      labelId={`select-${key}`}
                      value={formikProps.values[key]}
                      onBlur={formikProps.handleBlur}
                      onChange={formikProps.handleChange}
                    >
                      {[...fieldInfo._whitelist.list.values()].map(option => (
                        <MenuItem
                          key={option}
                          value={option}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </Select>
                    {hasError && <FormHelperText>{error}</FormHelperText>}
                  </FormControl>
                </Grid>
              );
            }

            const isMultiline: boolean = Boolean(path(['fieldsModifications', key, 'multiline'], props));

            return (
              <Grid item xs={12} key={key}>
                <TextField
                  id={key}
                  name={key}
                  type='text'
                  fullWidth
                  margin='dense'
                  label={fieldNameToLabel(key)}
                  error={hasError}
                  multiline={isMultiline}
                  rowsMax={isMultiline ? 3 : 1}
                  helperText={hasError ? error : ''}
                  value={formikProps.values[key]}
                  onBlur={formikProps.handleBlur}
                  onChange={formikProps.handleChange}
                />
              </Grid>
            );
          })}
        </Grid>
      )}
    </FormikConsumer>
  )
}

export default FormikFields;