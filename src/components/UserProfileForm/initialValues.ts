import { IUserProfile } from '../../interfaces/IUserProfile';

export const initialValues: IUserProfile = {
  username: '',
  gender: '',
  firstName: '',
  lastName: '',
  birthDate: new Date('01-01-1990'),
  email: '',
  phoneNumber: '',
  website: '',
  aboutMe: '',
  address: '',
  country: '',
  city: '',
  zip: ''
};