import React, { ReactComponentElement, useState } from 'react';
import { Observer } from 'mobx-react';
import { Form, Formik, FormikProps, FormikProvider } from 'formik';
import { IconButton, LinearProgress } from '@material-ui/core';
import RefreshIcon from '@material-ui/icons/Refresh';
import { IUserProfile } from '../../interfaces/IUserProfile';
import GeneralInfoStep from '../UserProfileFormSteps/GeneralInfoStep';
import { generalInfoSchema } from '../../yupSchemas/userProfile/generalInfoSchema';
import { contactsInfoSchema } from '../../yupSchemas/userProfile/contactsInfoSchema';
import { addressInfoSchema } from '../../yupSchemas/userProfile/addressInfoSchema';
import ContactsInfoStep from '../UserProfileFormSteps/ContactsInfoStep';
import AddressInfoStep from '../UserProfileFormSteps/AddressInfoStep';
import StepperStore from '../../stores/StepperStore';
import { StepperContext } from './StepperContext';
import { initialValues } from './initialValues';
import './styles.sass';

const stepStrategy: { [key: number]: ReactComponentElement<any> } = {
  0: <GeneralInfoStep />,
  1: <ContactsInfoStep />,
  2: <AddressInfoStep />,
}

const validationStrategy: { [key: number]: any } = {
  0: generalInfoSchema,
  1: contactsInfoSchema,
  2: addressInfoSchema
}

function UserProfileForm() {
  const [stepper] = useState(() => new StepperStore(3));

  return (
    <Observer>
      {() => (
        <Formik
          initialValues={initialValues}
          validationSchema={validationStrategy[stepper.step]}
          onSubmit={(values: IUserProfile) => {
            console.log(values);
          }}
        >
          {(formikProps: FormikProps<IUserProfile>) => (
            <Form className='profile-form'>
              <FormikProvider value={formikProps}>
                <StepperContext.Provider value={stepper}>
                  <IconButton
                    aria-label="reset form"
                    className='profile-form__reset-button'
                    onClick={() => {
                      stepper.reset();
                      formikProps.handleReset();
                    }}
                  >
                    <RefreshIcon />
                  </IconButton>
                  <LinearProgress variant="determinate" value={(stepper.step / (stepper.totalSteps - 1)) * 100} />
                  {stepStrategy[stepper.step]}
                </StepperContext.Provider>
              </FormikProvider>
            </Form>
          )}
        </Formik>
      )}
    </Observer>
  );
}

export default UserProfileForm;