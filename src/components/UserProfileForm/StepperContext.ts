import { createContext } from 'react';
import StepperStore from '../../stores/StepperStore';

export const StepperContext = createContext(new StepperStore(0));