import React from 'react';
import { FormikConsumer, FormikProps } from 'formik';
import { Button, Grid } from '@material-ui/core';
import { StepperContext } from '../UserProfileForm/StepperContext';

function StepperButtons() {
  return (
    <FormikConsumer>
      {(formikProps: FormikProps<any>) => (
        <Grid container alignItems='center' justify='space-between'>
            <StepperContext.Consumer>
              {(stepper) => {
                const hasErrors: boolean = Object.keys(formikProps.errors).length !== 0;
                const wasTouched: boolean = Object.values(formikProps.touched).every(touched => touched === false);
                const isLastStep: boolean = stepper.step === stepper.totalSteps - 1;

                return (
                  <>
                    <Grid item xs={stepper.step === 0 ? 2 : 'auto'}>
                      {stepper.step !== 0 && (
                        <Button
                          color='primary'
                          variant='outlined'
                          onClick={stepper.stepBackward}
                        >
                          Back
                        </Button>
                      )}
                    </Grid>
                    <Grid item>
                      {`${stepper.step + 1} / ${stepper.totalSteps}`}
                    </Grid>
                    <Grid item>
                      <Button
                        color='primary'
                        variant='contained'
                        disabled={wasTouched || hasErrors}
                        type={isLastStep ? 'submit' : 'button'}
                        onClick={() => {
                          if (!isLastStep) {
                            stepper.stepForward();
                          }
                        }}
                      >
                        {isLastStep ? 'Finish' : 'Forward'}
                      </Button>
                    </Grid>
                  </>
                );
              }}
            </StepperContext.Consumer>
        </Grid>
      )}
    </FormikConsumer>
  );
}

export default StepperButtons;