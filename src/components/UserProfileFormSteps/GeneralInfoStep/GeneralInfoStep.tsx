import React from 'react';
import FormikFields from '../../FormikFields';
import { Paper } from '@material-ui/core';
import { generalInfoSchema } from '../../../yupSchemas/userProfile/generalInfoSchema';
import StepperButtons from '../../StepperButtons/StepperButtons';

function GeneralInfoStep() {
  return (
    <Paper className='profile-form__wrapper'>
      <h2 className='profile-form__title'>General Info</h2>
      <FormikFields
        className='profile-form__fields'
        fields={Object.entries(generalInfoSchema.fields)}
      />
      <StepperButtons />
    </Paper>
  );
}

export default GeneralInfoStep;