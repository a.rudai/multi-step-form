import React from 'react';
import FormikFields from '../../FormikFields';
import { Paper } from '@material-ui/core';
import { addressInfoSchema } from '../../../yupSchemas/userProfile/addressInfoSchema';
import StepperButtons from '../../StepperButtons/StepperButtons';

function AddressInfoStep() {
  return (
    <Paper className='profile-form__wrapper'>
      <h2 className='profile-form__title'>Address Info</h2>
      <FormikFields
        className='profile-form__fields'
        fields={Object.entries(addressInfoSchema.fields)}
      />
      <StepperButtons />
    </Paper>
  );
}

export default AddressInfoStep;