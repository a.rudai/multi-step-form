import React from 'react';
import FormikFields from '../../FormikFields';
import { Paper } from '@material-ui/core';
import { contactsInfoSchema } from '../../../yupSchemas/userProfile/contactsInfoSchema';
import StepperButtons from '../../StepperButtons/StepperButtons';

function ContactsInfoStep() {
  return (
    <Paper className='profile-form__wrapper'>
      <h2 className='profile-form__title'>Contacts Info</h2>
      <FormikFields
        className='profile-form__fields'
        fields={Object.entries(contactsInfoSchema.fields)}
        fieldsModifications={{ aboutMe: { multiline: true } }}
      />
      <StepperButtons />
    </Paper>
  );
}

export default ContactsInfoStep;