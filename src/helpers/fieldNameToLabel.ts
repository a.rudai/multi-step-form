export const fieldNameToLabel = (fieldName: string): string =>
  fieldName
    .replace(/[A-Z]/g, (letter: string) => ` ${letter}`)
    .replace(/^\w/g, (letter: string) => letter.toUpperCase());