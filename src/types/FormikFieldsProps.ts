export type FormikFieldsProps = {
  fields: [string, object | any][],
  className?: string,
  fieldsModifications?: {
    [key: string]: {
      multiline: boolean
    }
  }
};