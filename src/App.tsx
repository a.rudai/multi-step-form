import React from 'react';
import UserProfileForm from './components/UserProfileForm';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import './App.sass';

function App() {
  return (
    <div className="App">
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <UserProfileForm />
      </MuiPickersUtilsProvider>
    </div>
  );
}

export default App;
