export interface IUserProfile {
  username: string,
  gender: '' | 'male' | 'female',
  firstName: string,
  lastName: string,
  birthDate: Date,

  email: string,
  phoneNumber: string,
  website: string,
  aboutMe: string,

  address: string,
  country: string,
  city: string,
  zip: string
}